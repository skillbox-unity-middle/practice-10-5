using System;
using UnityEngine;

public class GameBalance : MonoBehaviour, ISettings
{
    [SerializeField] Settings _settings;

    public int HeroHealth => _settings.heroHealth;
    public int RecoverHealth => _settings.recoverHealth;

    public event Action onLoadSettings;

    private GameBalance()
    {
        onLoadSettings?.Invoke();
    }
}

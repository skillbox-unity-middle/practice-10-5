﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Zenject;
using DG.Tweening;

public class ApplyToHP : MonoBehaviour, IAbilityTarget, IConvertGameObjectToEntity
{
    public List<GameObject> Targets { get; set; }

    private ISettings _settings;
    private int _hp;
    private EntityManager _dstManager;

    [Inject]
    public void Init(ISettings settings)
    {
        _settings = settings;
    }

    private void Awake()
    {
        _settings.onLoadSettings += SetRecoverHp;
    }

    public void Execute(Entity entity)
    {
        foreach (var target in Targets)
        {
            var health = target.GetComponent<CharacterHealthConvertToEntity>();
            if (health != null)
            {
                health.SetCharacterHealthEntity(_hp);
                _hp = 0;
                Destroy(gameObject);
                _dstManager.DestroyEntity(entity);
            }
        }
    }

    private void SetRecoverHp()
    {
        _hp = _settings.RecoverHealth;
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        _dstManager = dstManager;
        _dstManager.AddComponentData(entity, new HealthRecoverAnimData());
    }
}

